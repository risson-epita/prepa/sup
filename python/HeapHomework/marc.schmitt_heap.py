__license__ = 'Nathalie (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: heap.py 2018-02-13'

"""
Heap homework
2018
@author: marc.schmitt
"""


# given function


def newHeap():
    """ returns a fresh new empty heap

       :rtype: list (heap)
    """
    return [None]


# ---------------------------------------------------------------


def isEmpty(H):
    """ tests if H is empty

       :param H: the heap
       :rtype: bool
    """
    return H == [None]


def heapPush(H, elt, val):
    """ adds the pair (val, elt) to heap H and returns the heap modified

       :param H: The heap
       :param elt: The element to add
       :param val: The value associated to elt
       :rtype: list  (heap)
    """

    H.append((val, elt))
    return heapify(H)


def heapPop(H):
    """ returns and deletes the element of smallest value

       :param H: The heap
       :rtype: any (the removed element)

    """
    r = None

    if not isEmpty(H):
        r = H[1][1]
        for i in range(1, len(H) - 1):
            H[i] = H[i + 1]
        H.pop()

    return r


# ---------------------------------------------------------------


def isHeap(T):
    """ tests whether T is a heap

       :param T: list (a complete tree)
       :rtype: bool

    """
    if isEmpty(T):
        return True

    elif T[0] is not None:
        return False

    else:
        is_heap = True
        n = len(T)
        for i in range(1, n):
            if i * 2 >= n or i * 2 + 1 >= n:
                break
            else:
                if T[i] is None:
                    is_heap = False
                elif 2 * i < n and (T[i][0] > T[2 * i][0] or T[2 * i] is None):
                    is_heap = False
                elif 2 * i + 1 < n and (T[i][0] > T[2 * i + 1][0] or T[2 * i + 1] is None):
                    is_heap = False
        return is_heap


def heapify(H):
    """ makes H a heap (in place) - returns H modified

      :param H: list (a complete tree)
      :rtype: list (a heap)
    """
    if isEmpty(H) or H == []:
        return H
    else:
        if H[0] is not None:
            H = [None] + H
            print("heapify testinginginging", H)
        n = len(H)
        while n != 0:
            newn = 0
            for i in range(2, n):
                if H[i - 1][0] > H[i][0]:
                    H[i - 1], H[i] = H[i], H[i - 1]
                    newn = i
            n = newn
        return H
