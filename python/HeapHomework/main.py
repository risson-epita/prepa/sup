import time
import login_heap

def main():
    #################################### TEST ############################

    T10 = [None, (0, 'val_0'), (2, 'val_2'), (5, 'val_5'), (3, 'val_3'), (1, 'val_1'), (6, 'val_6'), (9, 'val_9'), (4, 'val_4'), (7, 'val_7'), (8, 'val_8')]
    T50 = [None, (33, 'val_33'), (45, 'val_45'), (47, 'val_47'), (19, 'val_19'), (31, 'val_31'), (38, 'val_38'), (17, 'val_17'), (15, 'val_15'), (23, 'val_23'), (1, 'val_1'), (24, 'val_24'), (28, 'val_28'), (26, 'val_26'), (8, 'val_8'), (43, 'val_43'), (39, 'val_39'), (7, 'val_7'), (16, 'val_16'), (13, 'val_13'), (44, 'val_44'), (29, 'val_29'), (40, 'val_40'), (42, 'val_42'), (6, 'val_6'), (2, 'val_2'), (10, 'val_10'), (0, 'val_0'), (27, 'val_27'), (36, 'val_36'), (5, 'val_5'), (20, 'val_20'), (25, 'val_25'), (46, 'val_46'), (35, 'val_35'), (32, 'val_32'), (22, 'val_22'), (30, 'val_30'), (11, 'val_11'), (48, 'val_48'), (34, 'val_34'), (4, 'val_4'), (41, 'val_41'), (14, 'val_14'), (12, 'val_12'), (49, 'val_49'), (18, 'val_18'), (3, 'val_3'), (9, 'val_9'), (21, 'val_21'), (37, 'val_37')]
    T100 = [None, (34, 'val_34'), (81, 'val_81'), (59, 'val_59'), (56, 'val_56'), (79, 'val_79'), (66, 'val_66'), (12, 'val_12'), (63, 'val_63'), (37, 'val_37'), (27, 'val_27'), (24, 'val_24'), (5, 'val_5'), (65, 'val_65'), (62, 'val_62'), (87, 'val_87'), (51, 'val_51'), (85, 'val_85'), (49, 'val_49'), (78, 'val_78'), (25, 'val_25'), (74, 'val_74'), (60, 'val_60'), (67, 'val_67'), (57, 'val_57'), (29, 'val_29'), (4, 'val_4'), (13, 'val_13'), (0, 'val_0'), (88, 'val_88'), (91, 'val_91'), (44, 'val_44'), (10, 'val_10'), (36, 'val_36'), (71, 'val_71'), (23, 'val_23'), (93, 'val_93'), (80, 'val_80'), (70, 'val_70'), (84, 'val_84'), (55, 'val_55'), (18, 'val_18'), (96, 'val_96'), (32, 'val_32'), (89, 'val_89'), (76, 'val_76'), (38, 'val_38'), (86, 'val_86'), (43, 'val_43'), (52, 'val_52'), (3, 'val_3'), (92, 'val_92'), (61, 'val_61'), (77, 'val_77'), (35, 'val_35'), (2, 'val_2'), (54, 'val_54'), (6, 'val_6'), (53, 'val_53'), (97, 'val_97'), (90, 'val_90'), (50, 'val_50'), (46, 'val_46'), (9, 'val_9'), (14, 'val_14'), (58, 'val_58'), (22, 'val_22'), (19, 'val_19'), (17, 'val_17'), (21, 'val_21'), (42, 'val_42'), (47, 'val_47'), (15, 'val_15'), (39, 'val_39'), (26, 'val_26'), (69, 'val_69'), (11, 'val_11'), (75, 'val_75'), (99, 'val_99'), (1, 'val_1'), (94, 'val_94'), (83, 'val_83'), (72, 'val_72'), (16, 'val_16'), (33, 'val_33'), (30, 'val_30'), (73, 'val_73'), (48, 'val_48'), (41, 'val_41'), (20, 'val_20'), (82, 'val_82'), (40, 'val_40'), (28, 'val_28'), (64, 'val_64'), (8, 'val_8'), (68, 'val_68'), (7, 'val_7'), (45, 'val_45'), (95, 'val_95'), (98, 'val_98'), (31, 'val_31')]

    testfailed = []
    start = time.time()

    print("\n============================================================================\n      Heap Test  \n")

    print("\n============================================================================\n    isEmpty new heap ?\n")
    print("Expected : True\n")
    empty = login_heap.isEmpty(login_heap.newHeap())
    if not empty:
        testfailed.append("Empty test")
    print(empty)

    print("\n============================================================================\n    isHeap ?\n")
    print("Expected : all False\n")
    print("T10 : ", login_heap.isHeap(T10))
    print("T50 : ",login_heap.isHeap(T50))
    print("T100 : ",login_heap.isHeap(T100))
    if login_heap.isHeap(T10) or login_heap.isHeap(T50) or login_heap.isHeap(T100):
        testfailed.append("isHeap test")
    T10 = login_heap.heapify(T10)
    T50 = login_heap.heapify(T50)
    T100 = login_heap.heapify(T100)

    print("\n============================================================================\n    isHeap after heapify ?\n")
    print("Expected : all True\n")
    print("T10 : ",login_heap.isHeap(T10))
    print("T50 : ",login_heap.isHeap(T50))
    print("T100 : ",login_heap.isHeap(T100))
    if not (login_heap.isHeap(T10) and login_heap.isHeap(T50) and login_heap.isHeap(T100)):
        testfailed.append("isHeap test after heapify")

    print("\n============================================================================\n    Heapify result ?\n")
    print("T10 : ",T10)
    print("T50 : ",T50)
    print("T100 : ",T100)

    print("\n============================================================================\n    HeapPop ?\n")
    print("Expected : all val_0\n")
    result10 = login_heap.heapPop(T10)
    result50 = login_heap.heapPop(T50)
    result100 = login_heap.heapPop(T100)
    print("T10 min elt : ",result10)
    print("T50 min elt : ",result50)
    print("T100 min elt : ",result100)
    if not (result10 == "val_0" and result50 == "val_0"  and result100 == "val_0"):
        testfailed.append("Heap pop test")

    print("\nExpected : 10 50 100\n")
    print("T10 lenght : ",len(T10))
    print("T50 lenght : ",len(T50))
    print("T100 lenght : ",len(T100))
    if not (len(T10) == 10 and len(T50) == 50 and len(T100) == 100):
        testfailed.append("Heap pop lenght test")

    print("\n============================================================================\n    HeapPush ?\n")
    print("Expected : all (0, 'val_0')\n")
    result10 = login_heap.heapPush(T10, 'val_0', 0)
    result50 = login_heap.heapPush(T50, 'val_0', 0)
    result100 = login_heap.heapPush(T100, 'val_0', 0)
    print("T10[1] : ",result10[1])
    print("T50[1] : ",result50[1])
    print("T100[1] : ",result100[1])
    if not (result10[1] == (0, 'val_0') and result50[1] == (0, 'val_0')  and result100[1] == (0, 'val_0')):
        testfailed.append("HeapPush val_0 test")

    print("\nExpected : 11 51 101\n")
    print("T10 lenght : ",len(T10))
    print("T50 lenght : ",len(T50))
    print("T100 lenght : ",len(T100))
    if not (len(T10) == 11 and len(T50) == 51 and len(T100) == 101):
        testfailed.append("Heap push lenght test")

    print("\n============================================================================\n    isHeap after heap push ?\n")
    print("Expected : all True\n")
    print("T10 : ",login_heap.isHeap(T10))
    print("T50 : ",login_heap.isHeap(T50))
    print("T100 : ",login_heap.isHeap(T100))
    if not (login_heap.isHeap(T10) and login_heap.isHeap(T50) and login_heap.isHeap(T100)):
        testfailed.append("isHeap test after heap push")


    print("\n============================================================================\n    HeapPop too much ?\n")
    print("Expected : [None] 1\n")
    for i in range(151):
        login_heap.heapPop(T100)
    print("T100 result : ", T100)
    print("T100 lenght : ",len(T100))
    if not (T100 == login_heap.newHeap() and len(T100) == 1):
        testfailed.append("heap pop too much test")

    timetest = time.time() - start
    print("\n============================================================================\n    Heap test failed list\n")
    if testfailed == []:
        print("\n No test failed ... \n All test was completed !")
    else:
        for e in testfailed:
            print("-", e)

    print("\n[+] login_heap test in {} s".format(str(timetest)))


if __name__ == "__main__":
    main()
