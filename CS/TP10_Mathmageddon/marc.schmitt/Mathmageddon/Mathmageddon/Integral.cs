﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace integral
{
    class Integral
    {
        public static double IntegralRectangleRule(double a, double b, Func<double, double> f)
        {
            return f(a) * (b - a);
        }

        public static double CompositeIntegralRectangleRule(double a, double b, double n, Func<double, double> f)
        {
            double inferior = 0;
            double superior = 0;
            for (var i = 0; i < (b - a) / n; i++)
            {
                inferior += f(a + n * i);
                superior += f(a + n * (i + 1));
            }
            return (inferior + superior) * n / 2;
        }

        public static double CIRRErrorMargin(double a, double b, double n, Func<double, double> f, Func<double, double> F)
        {
            return (F(b) - F(a)) - CompositeIntegralRectangleRule(a, b, n, f);
        }

        public static double IntegralTrapezoidalRule(double a, double b, Func<double, double> f)
        {
            return (b - a) * ((f(a) + f(b)) / 2);
        }

        public static double CompositeIntegralTrapezoidalRule(double a, double b, double n, Func<double, double> f)
        {
            double result = 0;
            for (var i = 0; i < (b - a) / n; i++)
            {
                result += f(a + n * i) + f(a + n * (i + 1));
            }
            return result * n / 2;
        }

        public static double CITRErrorMargin(double a, double b, double n, Func<double, double> f, Func<double, double> F)
        {
            return (F(b) - F(a)) - CompositeIntegralTrapezoidalRule(a, b, n, f);
        }

        public static double IntegralSimpsonRule(double a, double b, Func<double, double> f)
        {
            return ((b - a) / 6) * (f(a) + 4 * f((a + b) / 2) + f(b));
        }

        public static double CompositeIntegralSimpsonRule(double a, double b, double n, Func<double, double> f)
        {
            double si1 = 0;
            double si2 = 0;
            
            for (var i = 1; i <= ((b - a) / n) / 2 - 1; i++)
            {
                si1 += f(a + 2 * i * n);
            }

            for (var i = 1; i <= ((b - a) / n) / 2; i++)
            {
                si2 += f(a + (2 * i - 1) * n);
            }

            return (n / 3) * (f(a) + 2 * si1 + 4 * si2 + f(b));
        }

        public static double CISRErrorMargin(double a, double b, double n, Func<double, double> f, Func<double, double> F)
        {
            return (F(b) - F(a)) - CompositeIntegralSimpsonRule(a, b, n, f);
        }
    }
}