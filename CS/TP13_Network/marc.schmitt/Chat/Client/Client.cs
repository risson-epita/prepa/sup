﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Client
{
	public class Client
	{
		private readonly Socket _sock;

		public void Run()
		{
			new Thread(() =>
			{
				while (true) ReceiveData();
			}) {IsBackground = true}.Start();

			while (true) SendData();
		}

		public Client(IPAddress address, int port)
		{
			_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			_sock.Connect(address, port);
		}

		public void ReceiveData()
		{
			var bytes = new byte[1024];
			var size = _sock.Receive(bytes, SocketFlags.None);
			var str = Encoding.ASCII.GetString(bytes).Substring(0,1024);
			Console.WriteLine(str);
		}

		public void SendData()
		{
			var input = Console.ReadLine();
			var bytes = Encoding.ASCII.GetBytes(input.Substring(0,1024));
			_sock.Send(bytes, SocketFlags.None);
		}
	}
}