﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace TP1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //5.1: Exercise 0
            //HelloWorlds(5);
            
            //5.2: Exercise 1
            //Echo();
            
            //5.3: Exercise 2
            //Reverse();
            
            //5.4: Exercise 3
            //LoveAcdc();
            
            //5.5: Exercise 4
            //MCQ("Are those TPs fun ?", "Yes", "Of course", "42", "La réponse D", 3);
            
            //5.6: Exercise 5
            //BestYears();
            
            //5.7: Exercise 6
            //Morse("_ .. __ . _ ___ .__. ._.. ._ _.__ _ .... . __. ._ __ . ._._._");
            
            //5.8: Exercise 7
            //Lighthouse(12);
            //Lighthouse(1);
            //Lighthouse(0);
            
            //BONUS: my sl
            //MySl();
            
            //BONUS: LightHouse++
            //LightHouseBonus(7);
            //LightHouseBonus(1);
            //LightHouseBonus(0);
            
        }

        // Displays (exactly) "Hello (West)World!" n times.
        public static void HelloWorlds(int n)
        {
            for (var i = 0; i < n; i++)
            {
                Console.WriteLine("Hello (West)World!");
            }
        }

        // Repeats a user input.
        public static void Echo()
        {
            var input = Console.ReadLine();
            Console.WriteLine("\n{0}", input);
        }

        // Function used by Reverse in order to do recursion. Thus, it is private.
        private static void ReverseRec(string s)
        {
            if (s.Length > 1)
            {
                ReverseRec(s.Substring(1));
                Console.Write(s[0]);
            }
            else
            {
                Console.Write(s[0]);
            }
        }

        // Reverses a user input and prints it.
        public static void Reverse()
        {
            var input = Console.ReadLine();
            Console.WriteLine();
            ReverseRec(input);
            Console.WriteLine();
        }

        // Prints a love message <3.
        public static void LoveAcdc()
        {
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.ForegroundColor = ConsoleColor.White;
            var message = new List<string>
            {
                "  * * *   * * *  ",
                "* *   * * *   * *",
                "*       *       *",
                "* *  <3 ACDC  * *",
                "  * *       * *  ",
                "    * *   * *    ",
                "      * * *      ",
                "        *        "
            };
            foreach (var line in message)
            {
                Console.WriteLine(line);
            }
            Console.ResetColor();
        }

        // This is an aditionnal function, used in other functions of this TP. And thus, it is private.
        // Checks if the string provided can be converted into an integer
        // if so it gives you this integer
        // otherwise, it raises an exception with the provided error message 
        private static int ValidIntInput(string input, string errorMessage)
        {
            try
            {
                return int.Parse(input);
            }
            catch (FormatException)
            {
                throw new Exception(errorMessage);
            }
        }
        
        // Recreates a MCQ and checks if the user answer if correct.
        public static void MCQ(string question, string aws1, string aws2, string aws3, string aws4, int aws)
        {
            Console.WriteLine("{0}\n1) {1}\n2) {2}\n3) {3}\n4) {4}", question, aws1, aws2, aws3, aws4);
            var input = ValidIntInput(Console.ReadLine(), "So writing a number is too hard, n00b...");
            if (input == aws)
            {
                Console.WriteLine("Good job bro!");
            }
            else if (0 < input && input < 5)
            {
                Console.WriteLine("You lose... The answer was {0}.", aws);
            }
            else
            {
                Console.WriteLine("So couting is too hard, n00b...");
            }
        }

        // Displays by year wether Epita graduating classes are good or bad. Starts in 1984 and ends in 2022.
        public static void BestYears()
        {
            for (var i = 1989; i < 2023; i++)
            {
                switch (i)
                {
                        case 2020:
                            Console.WriteLine("Best Year");
                            break;
                        case 2022:
                            Console.WriteLine("Bad Year");
                            break;
                        default:
                            switch (i % 2)
                                {
                                    case 0:
                                        Console.WriteLine("Good Year");
                                        break;
                                    default:
                                        Console.WriteLine("Bad Year");
                                        break;
                                }
                            break;
                }

            }
        }

        // Converts a string into audio morse code.
        public static void Morse(string s, int i = 0)
        {
            switch (s[i])
            {
                    case '.':
                        Console.Beep(900, 150);
                        break;
                    case '_':
                        Console.Beep(900, 450);
                        break;
                    case ' ':
                        Thread.Sleep(450);
                        break;
                    default:
                        throw new Exception("Morse: invalid input: string can only contain underscores, dots or spaces.");
            }
            
            if (i != s.Length - 1)
            {
                Morse(s, ++i);
            }
        }
        
        // Additionnal function to build the lighthouse
        private static List<string> LightHouseBuilder(int n)
        {
            var groundFloor = new List<string> {" ===== ", "_||_||_", "-------"};

            var roof = new List<string>() {"  /^\\  ", "  |#|  "};

            var floor = new List<string>() {" |===| ", "  |0|  ", "  | |  "};
            
            var lightHouse = new List<string>();
            lightHouse.AddRange(roof);
            for (var i = 0; i < n; i++)
            {
                lightHouse.AddRange(floor);
            }
            lightHouse.AddRange(groundFloor);
            return lightHouse;
        }

        // Displays a nice and clean lignthouse with n floors.
        public static void Lighthouse(int n)
        {
            foreach (var line in LightHouseBuilder(n))
            {
                Console.WriteLine(line);
            }
        }

        // Mimics the sl command, only with a bigger mean of transport
        public static void MySl()
        {
            // C version of what is done in C#
            //signal(SIGINT, SIG_IGN);
            Console.TreatControlCAsInput = true;
            
            // Commented, because not working in linux
            // First, let's make our console big and clean
            //var previousWidth = Console.WindowWidth;
            //var previousHeight = Console.WindowHeight;
            //Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            //Console.WindowLeft = 0;
            //Console.WindowTop = 0;
            //eConsole.SetWindowPosition(0, 0);
            Console.Clear();
            
            int counter;
            
            //Creation of two lists containing all the lines for the rocket and all the lines for the tower
            var tower = new List<string>();
            counter = 0;
            string lineTower;
            var fileTower = new StreamReader(@"tower.ascii");
            while ((lineTower = fileTower.ReadLine()) != null)
            {
                tower.Add(lineTower);
                counter++;
            }
            fileTower.Close();

            counter = 0;
            var rocket = new List<string>();
            string lineRocket;
            var fileRocket = new StreamReader(@"rocket.ascii");
            while ((lineRocket = fileRocket.ReadLine()) != null)
            {
                rocket.Add(lineRocket);
                counter++;
            }
            fileRocket.Close();
            
            //Display of the first frame
            counter = 0;
            string line;
            var file = new StreamReader(@"frame1.ascii");
            while ((line = file.ReadLine()) != null)
            {
                Console.WriteLine(line);
                counter++;
            }
            file.Close();
            Thread.Sleep(350);
            
            //Display the other frames
            for (var i = 0; i < 68; i++)
            {
                Thread.Sleep(150);
                Console.SetCursorPosition(0, 0);
                for (var j = 0; j < 67; j++)
                {
                    if (i + j < 67)
                    {
                        Console.WriteLine(tower[j] + rocket[i + j]);
                    }
                    else
                    {
                        Console.WriteLine(tower[j]);
                    }
                }
                
            }

            //And at last, let's return things back to normal
            // Commented, because not working in linux
            //Console.SetWindowSize(previousWidth, previousHeight);
            Console.Clear();
        }

        // Generates a string containing n times the string provided
        private static string GenerateString(string s, int n, string sep=" ")
        {
            var r = "";
            for (var i = 0; i < n; i++)
            {
                r = r + sep + s;
            }
            return r;
        }

        // Displays a nice and clean lighthouse with n floors AND with a searchlight !
        public static void LightHouseBonus(int n)
        {
            var baseLightHouse = LightHouseBuilder(n);
            for (var i = 0; i < baseLightHouse.Count; i++)
            {
                Console.WriteLine(baseLightHouse[i] + GenerateString(" ", i, "") + GenerateString("_", i, ""));
            }
        }
    }
}