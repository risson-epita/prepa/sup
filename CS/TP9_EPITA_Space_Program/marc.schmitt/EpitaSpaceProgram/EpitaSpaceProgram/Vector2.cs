﻿using System;
using EpitaSpaceProgram.ACDC;

namespace EpitaSpaceProgram
{
    public class Vector2
    {
        public static readonly Vector2 Zero = new Vector2(0, 0);

        public Vector2(double x, double y)
        {
            if (double.IsInfinity(x) || double.IsNaN(x) || double.IsInfinity(y) || double.IsNaN(y))
                throw new ArgumentException("Invalid arguments to Vector2");
            X = x;
            Y = y;
        }

        public Vector2(Vector2 v)
        {
            if (double.IsInfinity(v.X) || double.IsNaN(v.X) || double.IsInfinity(v.Y) || double.IsNaN(v.Y))
                throw new ArgumentException("Invalid arguments to Vector2");
            X = v.X;
            Y = v.Y;
        }

        public double X { get; }
        public double Y { get; }

        public static Vector2 operator -(Vector2 v)
        {
            return new Vector2(-v.X, -v.Y);
        }

        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Vector2 operator *(double factor, Vector2 v)
        {
            return new Vector2(factor * v.X, factor * v.Y);
        }

        public static Vector2 operator *(Vector2 v, double factor)
        {
            return new Vector2(factor * v.X, factor * v.Y);
        }

        public static Vector2 operator /(double factor, Vector2 v)
        {
            return new Vector2(factor / v.X, factor / v.Y);
        }

        public static Vector2 operator /(Vector2 v, double factor)
        {
            return new Vector2(v.X / factor, v.Y / factor);
        }

        //TODO: can we use math?
        // Returns the norm of the vector.
        public double Norm()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        // Returns the normalized form of the vector.
        public Vector2 Normalized()
        {
            return Norm() != 0 ? new Vector2(this / Norm()): new Vector2(null);
        }

        // Returns the distance between two vectors.
        public static double Dist(Vector2 v1, Vector2 v2)
        {
            return Math.Sqrt(Math.Pow(v1.X - v2.X, 2) + Math.Pow(v1.Y - v2.Y, 2));
        }
        
        public static double Dot(Vector2 v1, Vector2 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        public override string ToString()
        {
            return "[" + Json.Escape(X) + ", " + Json.Escape(Y) + "]";
        }
    }
}