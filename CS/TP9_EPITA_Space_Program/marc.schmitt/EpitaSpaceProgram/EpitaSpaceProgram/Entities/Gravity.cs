﻿namespace EpitaSpaceProgram
{
    public class Gravity : Body
    {
        private Vector2 _initialPosition;
        private double _t;
        private Vector2 _gravity;
        
        public Gravity(string name, double mass, double density, Vector2 initialPosition, Vector2 gravity)
            : base(name, mass, density, initialPosition)
        {
            // Setting the initial position
            _initialPosition = initialPosition;
            
            // Here, we set the initial velocity of the body to None.
            // We could also have called the ctor. of the base class `Body` which accepts an initial velocity argument:
            // `base(name, mass, density, initialPosition, new Vector(00, 0))`
            Velocity = new Vector2(0, 0);
            
            // Setting the gravity
            _gravity = gravity;
        }

        public override void Update(double delta)
        {
            // Calling the base class' `Update` method as the very first step is extremely important, as our body would
            // not move properly otherwise.
            base.Update(delta);

            // Time goes on...
            _t += delta;
            
            // Modifying our Acceleration, according to earth's gravity
            Acceleration = _gravity;
        }
    }
}