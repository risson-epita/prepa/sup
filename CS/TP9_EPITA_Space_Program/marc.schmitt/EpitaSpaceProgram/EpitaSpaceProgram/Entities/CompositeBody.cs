﻿using System.Collections.Generic;

namespace EpitaSpaceProgram
{
    public abstract class CompositeBody : Body
    {
        private double _t;
        private List<Body> _bodies;
        
        protected CompositeBody(string name, double mass, double density, Vector2 initialPosition)
            : base(name, mass, density, initialPosition)
        {
            _bodies = new List<Body>();
        }

        protected void Add(Body body)
        {
            Velocity += body.Velocity;
            Acceleration += body.Acceleration;
            _bodies.Add(body);
        }

        public override void Update(double delta)
        {
            base.Update(delta);
            _t += delta;

            if (_bodies == null)
            {
                return;
            }

            foreach (var body in _bodies)
            {
                body.Update(delta);
                Acceleration += body.Acceleration;
            }
        }
    }
}