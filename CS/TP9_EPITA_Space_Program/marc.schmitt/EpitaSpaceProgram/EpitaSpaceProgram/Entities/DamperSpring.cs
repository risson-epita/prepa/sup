﻿namespace EpitaSpaceProgram
{
    public class DamperSpring : Spring
    {
        private double _t;
        private double _damping;
        private Vector2 _prevDisplacement;
        
        public DamperSpring(string name, double mass, double density, Vector2 initialPosition, Vector2 origin,
            double spring, double damping)
            : base(name, mass, density, initialPosition, origin, spring)
        {
            _damping = damping;
            _prevDisplacement = Vector2.Zero;
        }

        public override void Update(double delta)
        {
            var displacementVelocity = (Position - _prevDisplacement) / delta;
            base.Update(delta);
            _t += delta;
            var F_d = -1 * _damping * displacementVelocity;
            _prevDisplacement = Position;
            ApplyForce(F_d);
        }
    }
}