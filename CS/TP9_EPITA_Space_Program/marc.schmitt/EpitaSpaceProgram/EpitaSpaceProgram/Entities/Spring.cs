﻿namespace EpitaSpaceProgram
{
    public class Spring : Body
    {
        protected Vector2 InitialPosition;
        protected double T;
        protected Vector2 Origin;
        private double _spring;

        public Spring(string name, double mass, double density, Vector2 initialPosition, Vector2 origin, double spring)
            : base(name, mass, density, initialPosition)
        {
            InitialPosition = initialPosition;
            Origin = origin;
            _spring = spring;
        }

        public override void Update(double delta)
        {
            base.Update(delta);
            T += delta;

            var F_h = (-1) * _spring * (Position - Origin);
            ApplyForce(F_h);
        }
    }
}