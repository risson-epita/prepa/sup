﻿using System.Collections.Generic;
using EpitaSpaceProgram.ACDC;

namespace EpitaSpaceProgram
{
    //TODO: DO IT
    public class DistanceJoint : IEntity
    {
        private Vector2 _origin;
        private double _t;
        
        public DistanceJoint(Vector2 origin, Body body)
        {
            _origin = origin;
        }

        public void Update(double delta)
        {
            _t += delta;
        }

        // Don't change this method.
        public IEnumerable<string> Serialize()
        {
            return new List<string>();
        }
    }
}