﻿using System;
using System.Drawing;

namespace TinyPhotoshop
{
    public class InstaNashville : InstaFilter
    {
        public InstaNashville()
               : base("Nashville")
        {
        }

        public override Bitmap Process(Bitmap image)
        {
            return ColorTone(image, Color.FromArgb(246, 216, 176));
        }
    }
}