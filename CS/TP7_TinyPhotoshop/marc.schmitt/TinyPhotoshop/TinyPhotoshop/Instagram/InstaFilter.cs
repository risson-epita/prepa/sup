﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace TinyPhotoshop
{
    public abstract class InstaFilter : Filter
    {
        public InstaFilter(string name) : base(name)
        {
        }

	    public Bitmap ColorTone(Bitmap img, Color color)
	    {
		    var width = img.Width;
		    var height = img.Height;
		    var result = new Bitmap(width, height);
		    for (var i = 0; i < width; i++)
		    {
			    for (var j = 0; j < height; j++)
			    {
				    var oldColor = img.GetPixel(i, j);
				    var r = Convolution.Clamp(color.R * (255 / color.R));
				    var g = Convolution.Clamp(oldColor.G * (255 / color.G));
				    var b = Convolution.Clamp(oldColor.B * (255 / color.B));
				    img.SetPixel(i, j, Color.FromArgb(r, g, b));
			    }
		    }
		    return result;

		    
	    }

        //https://en.wikipedia.org/wiki/Vignetting

		public Bitmap Vignette(Bitmap b, Color c1, Color c2, float crop = 0.5f)
        {

			//FIXME
			throw new NotImplementedException();
		}
    }
}