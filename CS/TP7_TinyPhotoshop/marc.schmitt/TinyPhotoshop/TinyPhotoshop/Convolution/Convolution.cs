﻿using System;
using System.Drawing;

namespace TinyPhotoshop
{
    public static class Convolution
    {
	    //Those masks were found here: https://docs.gimp.org/en/plug-in-convmatrix.html
	    
        public static float[,] Gauss = {
            {1/9f, 2/9f, 1/9f},
            {2/9f, 4/9f, 2/9f},
            {1/9f, 2/9f, 1/9f}
        };
        
        public static float[,] Sharpen = {
	        {0, -1, 0},
	        {-1, 5, -1},
	        {0, -1, 0}
        };
        
        public static float[,] Blur = {
	        {1/2f, 1/2f, 1/2f},
	        {1/2f, 1, 1/2f},
	        {1/2f, 1/2f, 1/2f}
        };
        
        public static float[,] EdgeEnhance = {
	        {0, 0, 0},
	        {-1, 1, 0},
	        {0, 0, 0}
        };
        
        public static float[,] EdgeDetect = {
	        {0, 1, 0},
	        {1, -4, 1},
	        {0, 1, 0}
        };
        
        public static float[,] Emboss = {
	        {-2, -1, 0},
	        {-1, 1, 1},
	        {0, 1, 2}
        };
        
        public static int Clamp(float c)
        {
	        /*if (c <= 0)
	        {
		        return 0;
	        }
	        if (c >= 255)
	        {
		        return 255;
	        }
	        return c - (int) c >= 0.5 ? (int) c + 1 : (int) c;*/
	        return c <= 0 ? 0 : (c>= 255 ? 255 : (c - (int) c >= 0.5 ? (int) c + 1 : (int) c)); // sorry, not sorry
        }

        private static bool IsValid(int x, int y, Size size)
        {
	        return x < size.Width && y < size.Height && x >= 0 && y >= 0;
        }

	    private static bool IsValid(Point p, Size size)
	    {
		    return p.X < size.Width && p.Y < size.Height && p.X >= 0 && p.Y >= 0;
	    }

	    public static Image Convolute(Bitmap img, float[,] mask)
        {
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(img);
	        var size = new Size(width, height);
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        var neighbors = new []
			        {
				        new Point(i - 1, j - 1),
				        new Point(i - 1, j),
				        new Point(i - 1, j + 1),
				        new Point(i, j - 1),
				        new Point(i, j), 
				        new Point(i, j + 1),
				        new Point(i + 1, j - 1),
				        new Point(i + 1, j),
				        new Point(i + 1, j + 1)
			        };
			        float r = 0, g = 0, b = 0;
			        foreach (var pixel in neighbors)
			        {
				        if (IsValid(pixel, size))
				        {
					        var color = img.GetPixel(pixel.X, pixel.Y);
					        r += color.R * mask[i - pixel.X + 1, j - pixel.Y + 1];
					        g += color.G * mask[i - pixel.X + 1, j - pixel.Y + 1];
					        b += color.B * mask[i - pixel.X + 1, j - pixel.Y + 1];
				        }
			        }
			        result.SetPixel(i, j, Color.FromArgb(Clamp(r), Clamp(g), Clamp(b)));
		        }
	        }
	        return result;
		}
    }
}