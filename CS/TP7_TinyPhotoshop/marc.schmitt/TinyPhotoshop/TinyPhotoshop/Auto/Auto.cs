﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace TinyPhotoshop
{
    public static class Auto
    {

        public static Dictionary<char, int[]> GetHistogram(Bitmap img)
        {
            var hist = new Dictionary<char, int[]> 
                                           { { 'R', new int[256] },
                                             { 'G', new int[256] },
                                             { 'B', new int[256] }
                                           };
	        var width = img.Width;
	        var height = img.Height;
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        var pixel = img.GetPixel(i, j);
			        hist['R'][pixel.R]++;
			        hist['G'][pixel.G]++;
			        hist['B'][pixel.B]++;
		        }
	        }
	        return hist;
        }

		public static int FindLow(int[] hist)
		{
			if (hist.Length == 0)
			{
				return 0;
			}
			var low = hist[0];
			var index = 0;
			for (var i = 0; i < hist.Length; i++)
			{
				if (hist[i] < low)
				{
					low = hist[i];
					index = i;
				}
			}
			return index;
		}

		public static int FindHigh(int[] hist)
        {
	        if (hist.Length == 0)
	        {
		        return 0;
	        }
	        var high = hist[0];
	        var index = 0;
	        for (var i = 0; i < hist.Length; i++)
	        {
		        if (hist[i] > high)
		        {
			        high = hist[i];
			        index = i;
		        }
	        }
	        return index;
		}

		public static Dictionary<char, int> FindBound(Dictionary<char, int[]> hist, Func<int[], int> f)
        {
			var bound = new Dictionary<char, int>();
	        bound['R'] = f(hist['R']);
	        bound['G'] = f(hist['G']);
	        bound['B'] = f(hist['B']);      
	        return bound;
        }


		public static int[] ComputeLUT(int low, int high)
        {
			var LUT = new int[256];
	        for (var i = 0; i < LUT.Length; i++)
	        {
		        if (i < low)
		        {
			        LUT[i] = 0;
		        }
		        else if (i > high)
		        {
			        LUT[i] = 255;
		        }
		        else if (high - low == 0)
		        {
			        LUT[i] = 0;
		        }
		        else
		        {
			        LUT[i] = 255 * (i - low) / (high - low);
		        }
		        //LUT[i] = i < low ? 0 : (i > high ? 255 : 255 * (i - low) / (high - low));
	        }
	        return LUT;
        }

		public static Dictionary<char, int[]> GetLUT(Bitmap img)
        {
			var LUT = new Dictionary<char, int[]>();
	        var hist = GetHistogram(img);
	        var low = FindBound(hist, FindLow);
	        var high = FindBound(hist, FindHigh);
	        LUT['R'] = ComputeLUT(low['R'], high['R']);
	        LUT['G'] = ComputeLUT(low['G'], high['G']);
	        LUT['B'] = ComputeLUT(low['B'], high['B']);
			return LUT;
		}

        public static Image ConstrastStretch(Bitmap img)
        {
			var LUT = GetLUT(img);
	        var width = img.Width;
	        var height = img.Height;
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        var pixel = img.GetPixel(i, j);
			        var r = LUT['R'][pixel.R];
			        var g = LUT['G'][pixel.G];
			        var b = LUT['B'][pixel.B];
			        img.SetPixel(i, j, Color.FromArgb(r, g, b));
		        }
	        }
			return img;
		}

    }
}
