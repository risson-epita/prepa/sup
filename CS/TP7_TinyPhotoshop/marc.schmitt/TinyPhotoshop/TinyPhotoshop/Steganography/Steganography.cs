﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace TinyPhotoshop
{
    public static class Steganography
    {

        public static Image EncryptImage(Bitmap img, Bitmap enc)
        {
	        var width = enc.Width;
	        var height = enc.Height;
	        if (img.Height < enc.Height || img.Width < enc.Width)
	        {
		        throw new ArgumentException("enc is bigger than img", nameof(enc));
	        }
	        var result = new Bitmap(img);
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        var oldColor = img.GetPixel(i, j);
			        var encColor = enc.GetPixel(i, j);
			        /*var r = (oldColor.R / 16) * 16 + encColor.R / 16;
			        var g = (oldColor.G / 16) * 16 + encColor.G / 16;
			        var b = (oldColor.B / 16) * 16 + encColor.B / 16;*/
			        result.SetPixel(i, j,
				        Color.FromArgb((oldColor.R / 16) * 16 + encColor.R / 16,
					        (oldColor.G / 16) * 16 + encColor.G / 16,
					        (oldColor.B / 16) * 16 + encColor.B / 16));
		        }
	        }
	        return result;
		}

		public static Image DecryptImage(Bitmap img)
		{
			var width = img.Width;
			var height = img.Height;
			var result = new Bitmap(width, height);
			for (var i = 0; i < width; i++)
			{
				for (var j = 0; j < height; j++)
				{
					var oldColor = img.GetPixel(i, j);
					/*var r = (oldColor.R % 16) * 16;
					var g = (oldColor.G % 16) * 16;
					var b = (oldColor.B % 16) * 16;*/
					result.SetPixel(i, j, Color.FromArgb((oldColor.R % 16) * 16, (oldColor.G % 16) * 16, (oldColor.B % 16) * 16));
					//TODO: one liner
				}
			}
			return result;
		}

		public static Image EncryptText(Bitmap img, string text)
		{
			var width = img.Width;
			var height = img.Height;
			var length = text.Length * 2 + 2;
			if (length > width * height)
			{
				throw new ArgumentException("text is too long for this image", nameof(text));
			}

			var buffer = new int[length];
			int i;
			for (i = 0; i < text.Length; i++)
			{
				var cInt = (int) text[i];
				buffer[i * 2] = cInt / 16;
				buffer[i * 2 + 1] = cInt % 16;
			}
			buffer[length - 2] = 0;
			buffer[length - 1] = 0;
			
			var result = new Bitmap(width, height);
			var k = 0;
			for (i = 0; i < width; i++)
			{
				for (var j = 0; j < height; j++)
				{
					var color = img.GetPixel(i, j);
					if (k < length)
					{
						int r = color.R, g = color.G, b = color.B;
						switch (k % 3)
						{
							case 0:
								r = (color.R / 16) * 16 + buffer[k];
								break;
							case 1:
								g = (color.G / 16) * 16 + buffer[k];
								break;
							case 2:
								b = (color.B / 16) * 16 + buffer[k];
								break;
						}
						result.SetPixel(i, j, Color.FromArgb(r, g, b));
						k++;
					}
					else
						result.SetPixel(i, j, color);
				}
			}
			return result;
		}

	    //TODO: README
	    private static string ProcessBuffer(Queue<int> buffer)
	    {
		    var result = "";
		    while(buffer.Count > 2)
		    {
			    var strong = buffer.Dequeue();
			    var weak = buffer.Dequeue();
			    var c = (char) (strong * 16 + weak);
			    result += c;
		    }
		    return result;
	    }

		public static string DecryptText(Bitmap img)
		{
			var width = img.Width;
			var height = img.Height;
			var buffer = new Queue<int>();
			var previous0 = false;
			var p = 0;
			for (var i = 0; i < width; i++)
			{
				for (var j = 0; j < height; j++)
				{
					var c = 0;
					var color = img.GetPixel(i, j);
					switch (p % 3)
					{
						case 0:
							c = color.R % 16;
							break;
						case 1:
							c = color.G % 16;
							break;
						case 2:
							c = color.B % 16;
							break;
					}
					buffer.Enqueue(c);
					if (c != 0)
					{
						previous0 = false;
					}
					if (previous0 && c == 0)
					{
						return ProcessBuffer(buffer);
					}
					if (c == 0)
					{
						previous0 = true;
					}
					p++;
				}
			}
			return ProcessBuffer(buffer);
		}
    }
}