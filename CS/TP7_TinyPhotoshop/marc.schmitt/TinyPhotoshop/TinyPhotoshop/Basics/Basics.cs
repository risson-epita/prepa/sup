﻿using System;
using System.Drawing;

namespace TinyPhotoshop
{
    public static class Basics
    {
        public static Color Grey(Color color)
        {
            var grey = (int) (0.2125 * color.R + 0.7154 * color.G + 0.0721 * color.B);
            return Color.FromArgb(color.A, grey, grey, grey);
        }

        public static Color Binarize(Color color)
        {
            return (color.R + color.G + color.B) / 3 < 255 / 2
                ? Color.FromArgb(color.A, 0, 0, 0)
                : Color.FromArgb(color.A, 255, 255, 255);
        }

        public static Color BinarizeColor(Color color)
        {
            /*var r = color.R < 255 / 2 ? 0 : 255;
            var g = color.G < 255 / 2 ? 0 : 255;
            var b = color.B < 255 / 2 ? 0 : 255;*/
            return Color.FromArgb(color.A,
                color.R < 255 / 2 ? 0 : 255,
                color.G < 255 / 2 ? 0 : 255,
                color.B < 255 / 2 ? 0 : 255);
        }

        public static Color Negative(Color color)
        {
            /*var r = 255 - color.R;
            var g = 255 - color.G;
            var b = 255 - color.B;*/
            return Color.FromArgb(color.A,
                255 - color.R,
                255 - color.G,
                255 - color.B);
        }

        public static Color Tinter(Color color, Color tint, int factor)
        {
            /*var r = color.R - (color.R - tint.R * factor / 100) / 2;
            var g = color.G - (color.G - tint.G * factor / 100) / 2;
            var b = color.B - (color.B - tint.B * factor / 100) / 2;*/
            return Color.FromArgb(color.A,
                color.R - (color.R - tint.R * factor / 100) / 2,
                color.G - (color.G - tint.G * factor / 100) / 2,
                color.B - (color.B - tint.B * factor / 100) / 2);
        }

        public static Image Apply(Bitmap img, Func<Color, Color> func)
        {
            var width = img.Width;
            var height = img.Height;
            Image result = new Bitmap(width, height);
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    ((Bitmap) result).SetPixel(x, y, func(img.GetPixel(x, y)));
                }
            }
            return result;
        }
    }
}