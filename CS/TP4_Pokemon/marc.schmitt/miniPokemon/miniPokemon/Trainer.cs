﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace miniPokemon
{
    class Trainer : Animal
    {
        private List<Pokemon> listPokemon;
        private int age;
        public int Age
        {
            get => age;
            set => age = value;
        }


        #region Constructor

        public Trainer(string name, int age)
            : base(name)
        {
            Age = age;
            listPokemon = new List<Pokemon>();
        }

        #endregion Constructor

        
        #region Methods

        public override void WhoAmI()
        {
            Console.WriteLine("I'm a pokemon Trainer");
        }

        public int NumberOfPokemon()
        {
            return listPokemon.Count;
        }

        public override void Describe()
        {
            Console.WriteLine("My name is {0}, I'm {1}", Name, Age);
        }

        public void Birthday()
        {
            Age++;
        }

        public void MyPokemon()
        {
            Console.WriteLine("My Pokemon are :");
            foreach (var pokemon in listPokemon)
            {
                Console.WriteLine("- {0}", pokemon.Name);
            }
        }

        public void CatchAPokemon(Pokemon pokemon)
        {
            listPokemon.Add(pokemon);
        }

        #endregion Methods
    }
}