﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;

namespace miniPokemon
{
    public class Pokemon : Animal
    {
        public enum Poketype
        {
            POISON,
            FIRE,
            WATER,
            GRASS,
            ELECTRICK
        };

        private Poketype poketype;
        private int damage;
        private int level = 1;
        private bool isKO;
        private int life;
        
        public bool IsKo
        {
            get => life <= 0;
            private set => isKO = value;
        }
        public int Life
        {
            get => life;
            set
            {
                life = value;
                isKO = value <= 0;
            }
        }


        #region Constructor

        public Pokemon(string name, int life, int damage, Poketype poketype)
            : base(name)
        {
            Life = life;
            this.damage = damage;
            this.poketype = poketype;
        }

        #endregion Constructor
        

        #region Methods
        
        public override void WhoAmI()
        {
            Console.WriteLine("I'm a Pokemon");
        }

        public override void Describe()
        {
            Console.WriteLine("My name is {0} I'm a pokemon of type {1} and I'm level {2}", Name, poketype, level);
        }
        
        public void LevelUp()
        {
            level++;
        }

        public int Attack()
        {
            Console.WriteLine("{0} uses cut, it's supper effective", Name);
            return damage;
        }

        public void GetHurt(int damage)
        {
            if (Life - damage <= 0)
            {
                Life = 0;
            }
            else
            {
                Life -= damage;
            }
        }

        public void Heal(int life)
        {
            Life += life;
        }

        #endregion Methods
    }
}
