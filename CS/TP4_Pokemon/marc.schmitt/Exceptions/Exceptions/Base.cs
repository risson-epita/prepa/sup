﻿using System;

namespace Exceptions
{
    public static class Base
    {
        // Calculates the nth term of the Fibonacci sequence
        public static int Fibonacci(int n)
        {
            if (n < 0)
            {
                throw new ArgumentException("Input must be superior or equal to 0");
            }
            var m = 0;
            var p = 1;
            for (var i = 0; i < n; i++)
            {
                var s = m;
                m = p;
                p += s;
            }
            return m;
        }

        // Converts an angle in degrees to radians
        public static float DegToRad(float angle)
        {
            const float pi = (float) Math.PI;
            return (angle * pi / 180f) % (2 * pi);
        }
        
        // Converts an angle in radians to degrees
        public static float RadToDeg(float angle)
        {
            const float pi = (float) Math.PI;
            if (angle < -pi || angle > pi)
            {
                throw new ArgumentException("Input must be in between -PI and PI");
            }
            return angle * 180f / pi;
        }
                
        // Calculates n to the power of p
        public static float Pow(float n, int p)
        {
            switch (p)
            {
                case 0:
                    return 1;
                case 1:
                    return n;
            }
            try
            {
                var tot = n;
                for (var i = 1; i < Math.Abs(p); i++)
                {
                    tot *= n;
                }
                return p < 0 ? 1 / tot : tot;
            }
            catch (OverflowException)
            {
                throw new OverflowException("Too much numbers for me, sorry ;(");
            }
        }
    }
}