using System;

namespace TP0
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            4.1
//            Console.WriteLine("=====4.1:HelloWorld=====");
//            HelloWorld();

//            4.2
//            Console.WriteLine("=====4.2:SayHello=====");
//            SayHello();

//            4.3
//            Console.WriteLine("=====4.3:CalcAge=====");
//            CalcAge();

//            4.4
//            Console.WriteLine("=====4.4:Absolute=====");
//            Console.WriteLine(Absolute(-42));

//            4.5
//            Console.WriteLine("=====4.5:MyFact=====");
//            Console.WriteLine(MyFact(4));

//            4.6
//            Console.WriteLine("=====4.6:MyPow=====");
//            Console.WriteLine(MyPow(2, -1));

//            4.7
//            Console.WriteLine("=====4.7:MyFibo=====");
//            Console.WriteLine(MyFibo(10));
//            4.7.1
//            Console.WriteLine("=====4.7.1:ChangeChar=====");
//            Console.WriteLine(ChangeChar("hello", 'a', 1));
//            Console.WriteLine(ChangeChar("hello", 'n', 6));
//            4.7.2
//            Console.WriteLine("=====4.7.2:MyGcd=====");
//            Console.WriteLine(MyGcd(10, 15));
//            Console.WriteLine(MyGcd(2, 3));

//            4.8
//            4.8.1
//            Console.WriteLine("=====4.8.1:MySqrt=====");
//            Console.WriteLine(MySqrt(2, 1000));
//            4.8.2
//            Console.WriteLine("=====4.8.2:MyReverseString=====");
//            Console.WriteLine(MyReverseString("hello"));
//            4.8.3
//            Console.WriteLine("=====4.8.3:MyPalindrome=====");
//            Console.WriteLine(MyIsPalindrome("hello olleh"));
//            Console.WriteLine(MyIsPalindrome("kayak"));
//            Console.WriteLine(MyIsPalindrome("Kayak"));

//            4.9
//            Console.WriteLine("=====4.9:CalcRealAge=====");
//            CalcRealAge();
        }

        // Prints a simple "Hello World!"
        public static void HelloWorld()
        {
            Console.WriteLine("Hello World!");
        }
        
        // Says Hello to [name]
        public static void SayHello()
        {
            Console.WriteLine("What's your name?");
            Console.WriteLine("Well hello {0}!", Console.ReadLine());
        }

        // This is an aditionnal function, used in other functions of this TP. And thus, it is private.
        // Checks if the string provided can be converted into an integer
        // if so it gives you this integer
        // otherwise, it raises an exception with the provided error message 
        private static int ValidIntInput(string input, string errorMessage)
        {
            try
            {
                return int.Parse(input);
            }
            catch (FormatException)
            {
                throw new Exception(errorMessage);
            }
        }

        // Calculates from your year of birth an approximation of your age
        public static void CalcAge()
        {
            Console.WriteLine("What's your year of birth?");
            var birthYear = ValidIntInput(Console.ReadLine(),
                "CalcAge: invalid input: your year of birth must be an integer.");
            if (birthYear > DateTime.Now.Year)
            {
                throw new Exception(
                    "CalcAge: your input must be inferior to the current year. You have to be born yet to use this program");
            }
            else
            {
                //var age = DateTime.Now.Year - birthYear;
                Console.WriteLine("Looks like you're around {0}!", (DateTime.Now.Year - birthYear));
           }
        }

        // Returns the absolute value of an integer
        public static int Absolute(int x)
        {
            return x < 0 ? -x : x;
        }

        // Returns the factorial of a number
        public static uint MyFact(uint n)
        {
            uint count = 1;
            while (n > 0)
            {
                count *= n;
                n -= 1;
            }
            return count;
        }

        // Returns x to the power of n
        public static double MyPow(double x, int n)
        {
            if (n == 0)
            {
                return 1;
            }
            var i = Absolute(n);
            var y = x;
            while (i > 1)
            {
                y *= x;
                i -= 1;
            }
            return n < 0 ? 1 / y : y;
        }

        // Returns the nth term of the fibonnacie series
        public static uint MyFibo(uint n)
        {
            return n < 2 ? n : MyFibo(n - 1) + MyFibo(n - 2);
        }

        // Changes the nth character of the string s by c
        public static string ChangeChar(string s, char c, uint n)
        {
            var t = "";
            for (var i = 0; i < s.Length; i++)
            {
                if (i == n)
                {
                    return t + c + s.Substring(++i);
                }
                else
                {
                    t += s[i];
                }
            }
            return t;
        }

        // Returns the Greatest Common Divisor of two integers
        public static uint MyGcd(uint a, uint b)
        {
            while (b != 0)
            {
                var c = a;
                a = b;
                b = c % b;
            }
            return a;
        }

        // Returns an approximation of the sqrt of a number n over i iterations
        public static double MySqrt(double n, uint i)
        {
            if (i == 0)
            {
                return 0.5;
            }
            var p = MySqrt(n, i - 1);
            return (p + n / p) / 2;
        }

        // Reverses a string
        public static string MyReverseString(string s)
        {
            var r = "";
            for (var i = s.Length - 1; i >= 0; i--)
            {
                r += s[i];
            }
            return r;
        }

        // Checks whether or not a string is a palindrome 
        public static bool MyIsPalindrome(string s)
        {
            var i = 0;
            var j = s.Length - 1;
            while (j > i)
            {
                if (s[i] != s[j])
                {
                    return false;
                }
                i++;
                j--;
            }
            return true;
        }

        // Calculates your exact age from your day, month and year of birth
        public static void CalcRealAge()
        {
            Console.WriteLine("What's your year of birth?");
            var birthYear = ValidIntInput(Console.ReadLine(),
                "CalcRealAge: invalid input: your year of birth must be an integer.");
            Console.WriteLine("What's your month of birth?");
            var birthMonth = ValidIntInput(Console.ReadLine(),
                "CalcRealAge: invalid input: your month of birth must be an integer.");
            Console.WriteLine("What's your day of birth?");
            var birthDay = ValidIntInput(Console.ReadLine(),
                "CalcRealAge: invalid input: your day of birth must be an integer.");

            var birthDate = new DateTime(birthYear, birthMonth, birthDay);

            if (birthDate > DateTime.Now)
            {
                throw new Exception(
                    "CalcRealAge: your input must be inferior to the current day. You have to be born yet to use this program");
            }
            Console.WriteLine("Looks like you're exactly {0}!", (DateTime.Now - birthDate).Days / 365);
        }
    }
}