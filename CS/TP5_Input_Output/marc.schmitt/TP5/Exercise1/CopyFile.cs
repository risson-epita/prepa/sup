﻿using System;
using System.IO;

namespace Exercise1
{
	internal static class CopyFile
	{
		/// <summary>
		///   EXERCISE 1.3:
		///   <para />
		///   Read the source file and put its content into destination file
		/// </summary>
		/// <param name="source">the path to the source file</param>
		/// <param name="destination">the path to the destination file</param>
		public static void CopyAllFile(string source, string destination)
		{
			try
			{
				var lines = File.ReadAllLines(source);
				File.WriteAllLines(destination, lines);
			}
			catch (DirectoryNotFoundException)
			{
				Console.WriteLine("could not open file: {0}", source);
			}
		}

		/// <summary>
		///   EXERCISE 1.4:
		///   <para />
		///   Read the source file and put half of its content into destination file
		/// </summary>
		/// <param name="source">the path to the source file</param>
		/// <param name="destination">the path to the destination file</param>
		public static void CopyHalfFile(string source, string destination)
		{
			try
			{
				var lines = File.ReadAllLines(source);
				var halfLines = new string[lines.Length / 2];
				for (var i = 0 ; i < lines.Length / 2; i++)
				{
					halfLines[i] = lines[i];
				}
				File.WriteAllLines(destination, halfLines);
			}
			catch (DirectoryNotFoundException)
			{
				Console.WriteLine("could not open file: {0}", source);
			}
		}
	}
}