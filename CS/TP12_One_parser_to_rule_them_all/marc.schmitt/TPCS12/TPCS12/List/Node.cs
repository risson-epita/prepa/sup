﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace List
{
    public class Node<T>
    {
        protected T _data;
        protected Node<T> _next;
        protected Node<T> _prev;

        public Node(T elt)
        {
            _data = elt;
            _next = null;
            _prev = null;
        }

        public T Data
        {
            get => _data;
            set => _data = value;
        }

        public Node<T> Next
        {
            get => _next;
            set => _next = value;
        }

        public Node<T> Prev
        {
            get => _prev;
            set => _prev = value;
        }
    }
}