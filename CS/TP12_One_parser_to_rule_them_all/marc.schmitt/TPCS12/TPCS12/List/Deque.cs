﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace List
{
    public class Dequeue<T> : List<T>
    {
        public Dequeue()
            : base()
        {}
        
        public Dequeue(T elt)
            : base(elt)
        {}
        
        public T front()
        {
            return this[0];
        }
        
        public T back()
        {
            return this[Count - 1];
        }
        
        public virtual void popBack()
        {
            delete(Count - 1);
        }
        
        public void popFront()
        {
            delete(0);
        }

        public void pushFront(T elt)
        {
            insert(0, elt);
        }
        
        public void pushBack(T elt)
        {
            insert(Count, elt);
        }
    }
}