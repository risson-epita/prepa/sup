﻿using System;
using System.Collections.Generic;
using System.Data;

namespace EvalExpr
{
    public static class Parser
    {
        public static INode Parse(string expr)
        {
            var tokens = Lexer.Lex(expr);
            var tokensShuntingYarded = ShuntingYard(tokens);
            var ast = BuildAST(tokensShuntingYarded);
            var head = ast.Pop();
            head.Build(ast);
            return head;
        }

        private static Queue<Token> ShuntingYard(List<Token> tokens)
        {
            var output = new Queue<Token>();
            var operatorStack = new Stack<Token>();
            
            foreach (var token in tokens)
            {
                if (token.TokType == Token.Type.INT)
                {
                    output.Enqueue(token);
                }
                else
                {
                    while (operatorStack.Count != 0 &&
                           OperatorPrecedence(operatorStack.Peek()) >= OperatorPrecedence(token))
                    {
                        output.Enqueue(operatorStack.Pop());
                    }
                    operatorStack.Push(token);
                }
            }
            
            while(operatorStack.Count != 0)
            {
                output.Enqueue(operatorStack.Pop());
            }

            return output;
        }
        
        private static int OperatorPrecedence(Token token)
        {
            var precedence = -1;
            var c = token.Val;
            switch (c)
            {
                case "+":
                case "-":
                    precedence = 2;
                    break;
                case "*":
                case "/":
                    precedence = 3;
                    break;
                    
                default:
                    precedence = -1;
                    break;
            }
            return precedence;
        }

        private static Stack<INode> BuildAST(Queue<Token> tokens)
        {
            var output = new Stack<INode>();
            var n = tokens.Count;
            Token previous = null;
            for (var i = 0; i < n; i++)
            {
                var tok = tokens.Dequeue();
                if (tok.TokType == Token.Type.MINUS && (previous == null || previous.TokType != Token.Type.INT)) output.Push(new UnaryNode(false));
                else output.Push(Token2INode(tok));
                previous = tok;
            }
            return output;
        }

        private static INode Token2INode(Token token)
        {
            switch (token.TokType)
            {
                case Token.Type.INT:
                    return new ValueNode(int.Parse(token.Val));
                case Token.Type.PLUS:
                    return new BinaryNode(BinaryNode.Operator.PLUS);
                case Token.Type.MINUS:
                    return new BinaryNode(BinaryNode.Operator.MINUS);
                case Token.Type.MULT:
                    return new BinaryNode(BinaryNode.Operator.MULT);
                case Token.Type.DIV:
                    return new BinaryNode(BinaryNode.Operator.DIV);
                
                default:
                    return null;
            }
        }
    }
}