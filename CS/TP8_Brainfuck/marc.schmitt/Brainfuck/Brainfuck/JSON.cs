﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Brainfuck
{
    class JSONElement
    {
        public enum JSONType
        {
            DIC,
            LIST,
            STR,
            NB,
            BOOL,
            NULL
        }
        
        private JSONType type;
        
        public bool bool_value;
        public int int_value;
        public string string_value;
        public List<JSONElement> data;
        public List<string> key;

        public JSONElement(JSONType type)
        {
            this.type = type;
            if (type == JSONType.LIST || type == JSONType.DIC)
                data = new List<JSONElement>();
            if (type == JSONType.DIC)
                key = new List<string>();
        }

        public JSONType Type
        {
            get { return type; }
        }
        
    }

    static class JSON
    {
        // Generate a string containing n times the string provided
        private static string GenString(string s, int n, string sep="")
        {
            if (n <= 0)
            {
                return "";
            }
            var r = "";
            for (var i = 0; i < n; i++)
            {
                r = r + sep + s;
            }
            return r;
        }

        public static JSONElement.JSONType GetJsonType(char c)
        {
            switch (c)
            {
                case '"':
                    return JSONElement.JSONType.STR;
                case '[':
                    return JSONElement.JSONType.LIST;
                case '{':
                    return JSONElement.JSONType.DIC;
                case 't':
                    return JSONElement.JSONType.BOOL;
                case 'f':
                    return JSONElement.JSONType.BOOL;
                case 'n':
                    return JSONElement.JSONType.NULL;
                case '-':
                    return JSONElement.JSONType.NB;
            }
            int n;
            if (int.TryParse(c.ToString(), out n))
            {
                return JSONElement.JSONType.NB;
            }
            Console.WriteLine("prout");
            throw new ArgumentException("First character of JSON string is not a valid character", nameof(c));
        }

        public static string ParseString(string json, ref int index)
        {
            var result = "";
            index++;
            while (json[index] != '"' || json[index-1] == '\\')
            {
                result += json[index];
                index++;
            }
            index++; // to move the index after the last '"'
            return result;
        }


        public static int ParseInt(string json, ref int index)
        {
            var result = 0;
            int p;
            var neg = false;
            if (json[index] == '-')
            {
                neg = true;
                index++;
            }
            while (int.TryParse(json[index].ToString(), out p))
            {
                result = result * 10 + p;
                index++;
            }
            return neg ? (-1) * result : result;
        }

        public static bool ParseBool(string json, ref int index)
        {
            if (json[index] == 't')
            {
                index += 4;
                return true;
            }
            index += 5;
            return false;
        }
        

        public static void EatBlank(string json, ref int index)
        {
            while (json[index] == ' ' || json[index] == '\n' || json[index] == 11)
            {
                index++;
            }
        }

        public static string EatBlank(string json)
        {
            var result = "";
            for (var i = 0; i < json.Length; i++)
            {
                if (json[i] == '"')
                {
                    do
                    {
                        result += json[i];
                        i++;
                    } while (json[i] != '"' || json[i - 1] == '\\');
                    result += json[i];
                    continue;
                }
                if (json[i] == ' ' || json[i] == '\n')
                {
                    continue;
                }
                result += json[i];
            }
            return result;
        }

        public static JSONElement ParseJSONString(string json, ref int index)
        {
                EatBlank(json, ref index);
                var type = GetJsonType(json[index]);

                JSONElement result;
                switch (type)
                {
                    case JSONElement.JSONType.BOOL:
                        result = new JSONElement(JSONElement.JSONType.BOOL);
                        result.bool_value = ParseBool(json, ref index);
                        return result;
                    case JSONElement.JSONType.NB:
                        result = new JSONElement(JSONElement.JSONType.NB);
                        result.int_value = ParseInt(json, ref index);
                        return result;
                    case JSONElement.JSONType.STR:
                        result = new JSONElement(JSONElement.JSONType.NB);
                        result.string_value = ParseString(json, ref index);
                        return result;
                    case JSONElement.JSONType.NULL:
                        index += 4;
                        return new JSONElement(JSONElement.JSONType.NULL);
                }


                if (type == JSONElement.JSONType.DIC)
                {

                    result = new JSONElement(JSONElement.JSONType.DIC);
                    index++; // moving after the '{'
                    while (json[index] != '}')
                    {
                        EatBlank(json, ref index);
                        result.key.Add(ParseString(json, ref index));
                        EatBlank(json, ref index);
                        if (json[index] != ':')
                        {
                            throw new ArgumentException("JSON is not valid", nameof(json));
                        }
                        index++; // moving after the ':'
                        EatBlank(json, ref index);
                        result.data.Add(ParseJSONString(json, ref index));
                        EatBlank(json, ref index);
                        if (json[index] == ',')
                        {
                            index++; // moving after the ','
                        }
                        else if (json[index] != '}')
                        {
                            throw new ArgumentException("JSON is not valid", nameof(json));
                        }
                    }
                    index++; // moving after the '}'
                    return result;
                }

                if (type == JSONElement.JSONType.LIST)
                {
                    result = new JSONElement(JSONElement.JSONType.LIST);
                    index++; // moving after the '['
                    while (json[index] != ']')
                    {
                        EatBlank(json, ref index);
                        result.data.Add(ParseJSONString(json, ref index));
                        if (json[index] == ',')
                        {
                            index++; // moving after the ','
                        }
                        else if (json[index] != ']')
                        {
                            throw new ArgumentException("JSON is not valid", nameof(json));
                        }
                    }

                    index++; // moving after the ']'
                    return result;
                }
            throw new Exception("Something went very very wrong...");
        }
        
        public static JSONElement ParseJSONFile(string file)
        {
                var apoual = 0;
                var result =  ParseJSONString(EatBlank(File.ReadAllText(file)), ref apoual);
                return result;
        }

        public static void PrintJSON(JSONElement el, int indent = 0)
        {
            switch (el.Type)
            {
                case JSONElement.JSONType.BOOL:
                    Console.Write(el.bool_value ? "true" : "false");
                    break;
                case JSONElement.JSONType.NB:
                    Console.Write(el.int_value);
                    break;
                case JSONElement.JSONType.NULL:
                    Console.Write("null");
                    break;
                case JSONElement.JSONType.STR:
                    Console.Write("\"{0}\"", el.string_value);
                    break;
                
                case JSONElement.JSONType.LIST:
                    
                    Console.WriteLine("[");
                    indent += 4;
                    for (var i = 0; i < el.data.Count; i++)
                    {
                        Console.Write(GenString(" ", indent));
                        PrintJSON(el.data[i], indent);
                        if (i != el.data.Count - 1)
                        {
                            Console.WriteLine(",");
                        }
                    }
                    indent-=4;
                    Console.WriteLine();
                    Console.Write(GenString(" ", indent));
                    Console.Write("]");
                    break;
                    
                case JSONElement.JSONType.DIC:
                    Console.WriteLine("{");
                    indent += 4;
                    for (var i = 0; i < el.key.Count; i++)
                    {
                        Console.Write(GenString(" ", indent));
                        Console.Write("\"{0}\": ", el.key[i]);
                        PrintJSON(el.data[i], indent);
                        if (i != el.key.Count - 1)
                        {
                            Console.WriteLine(",");
                        }
                    }
                    indent -= 4;
                    Console.WriteLine();
                    Console.Write(GenString(" ", indent));
                    Console.Write("}");
                    break;
            }
        }

        public static JSONElement SearchJSON(JSONElement element, string key)
        {
            if (element.Type == JSONElement.JSONType.DIC)
            {
                for (var i = 0; i<element.key.Count;i++)
                {
                    if (key == element.key[i])
                    {
                        return element.data[i];
                    }
                    if (element.data[i].Type == JSONElement.JSONType.LIST ||
                        element.data[i].Type == JSONElement.JSONType.DIC)
                    {
                        var result = SearchJSON(element.data[i], key);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }
            
            else if (element.Type == JSONElement.JSONType.LIST)
            {
                foreach (var data in element.data)
                {
                    if (data.Type == JSONElement.JSONType.DIC ||
                        data.Type == JSONElement.JSONType.LIST)
                    {
                        var result = SearchJSON(data, key);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }
            return null;
        }
    }   
}