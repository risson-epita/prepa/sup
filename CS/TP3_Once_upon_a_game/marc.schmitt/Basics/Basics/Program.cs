﻿using System;

namespace Basics
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            /*
            // 3.1.1
            var a = 1;
            var b = 2;
            Reference.Swap(ref a, ref b);
            Console.WriteLine(a);
            Console.WriteLine(b);
            */
            
            /*
            //3.1.2
            var f = 3.1415F;
            Console.WriteLine(Reference.Trunc(ref f));
            Console.WriteLine(f);
            */
            
            
            /*
            //3.1.3
            var c = 'a';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = 'z';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = 'Z';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = 'A';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = '=';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = 'z';
            Reference.RotChar(ref c, -3);
            Console.WriteLine(c);
            c = 'Z';
            Reference.RotChar(ref c, -3);
            Console.WriteLine(c);
            c = 'a';
            Reference.RotChar(ref c, -3);
            Console.WriteLine(c);
            c = 'A';
            Reference.RotChar(ref c, -3);
            Console.WriteLine(c);
            c = '1';
            Reference.RotChar(ref c, -3);
            Console.WriteLine(c);
            c = '1';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            c = '8';
            Reference.RotChar(ref c, 3);
            Console.WriteLine(c);
            */
            
            
            
            //int[] hill ={ 1, 3, 4, 6, 8, 9, 7, 5, 3, 0 };
            
            //3.2.1
            //Console.WriteLine(Arrays.Search(hill, 4));
            
            //3.2.2
            //Console.WriteLine(Arrays.KingOfTheHill(hill));
            
            //3.2.3
            //PrintArr(Arrays.CloneArray(hill));
            
            //3.2.4
            //Arrays.BubbleSort(hill);
            //PrintArr(hill);
            //Arrays.CombSort(hill);
            //PrintArr(hill);
        }
        
        // Prints an Array of int
        public static void PrintArr(int[] arr)
        {
            foreach (var element in arr)
            {
                Console.Write(element + " ");
            }
            Console.WriteLine();
        }
    }
}