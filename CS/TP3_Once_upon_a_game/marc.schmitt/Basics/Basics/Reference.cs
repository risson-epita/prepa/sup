﻿using System;

namespace Basics
{
    public class Reference
    {
        // Swaps two variables
        public static void Swap(ref int a, ref int b)
        {
            var c = a;
            a = b;
            b = c;
        }

        // Returns the floor part of a float
        // and replaces the value of the variable by its deciaml part
        public static int Trunc(ref float f)
        {
            var floor = (int) f;
            f -= floor;
            return floor;
        }

        // Returns the type of a char:
        // if it's uppercase, lowercase or something else
        private static string CharType(char c)
        {
            var cInt = (int) c;
            if (cInt > 64 && cInt < 91)
            {
                return "upper";
            }
            else if (cInt > 96 && cInt < 123)
            {
                return "lower";
            }
            else if (cInt > 47 && cInt < 58)
            {
                return "number";
            }
            else
            {
                return "other";
            }
        }

        // Rotates a char n times
        public static void RotChar(ref char c, int n)
        {
            var cType = CharType(c);
            if (cType == "other")
            {
                return;
            }
            else if (cType == "number")
            {
                if ((int) c + n % 10 > 57)
                {
                    c = (char) (((int) c + (n % 10)) - 10);
                }
                else if ((int) c + n % 10 < 48)
                {
                    c = (char) (((int) c + (n % 10)) + 10);
                }
                else
                {
                    c = (char) ((int) c + (n % 10));
                }
            }
            else if (cType == "upper")
            {
                if ((int) c + n % 26 > 90)
                {
                    c = (char) (((int) c + (n % 26)) - 26);
                }
                else if ((int) c + n % 26 < 65)
                {
                    c = (char) (((int) c + (n % 26)) + 26);
                }
                else
                {
                    c = (char) ((int) c + (n % 26));
                }
            }
            else if (cType == "lower")
            {
                if ((int) c + n % 26 > 122)
                {
                    c = (char) (((int) c + (n % 26)) - 26);
                }
                else if ((int) c + n % 26 < 97)
                {
                    c = (char) (((int) c + (n % 26)) + 26);
                }
                else
                {
                    c = (char) ((int) c + (n % 26));
                }
            }
            else
            {
                throw new Exception("Something wrong appened, but shouldn't have...");
            }
        }
    }
}