﻿namespace Basics
{
    public class Arrays
    {
        // Returns the position of an element in an array
        // If the element is not int the array, returns -1
        public static int Search(int[] arr, int e)
        {
            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] == e)
                {
                    return i;
                }
            }
            return -1;
        }

        // Returns the king of the hill
        // If there isn't one, returns -1
        public static int KingOfTheHill(int[] arr)
        {
            for (var i = 1; i < arr.Length - 1; i++)
            {
                if (arr[i - 1] < arr[i] && arr[i] > arr[i + 1])
                {
                    return arr[i];
                }
            }
            return -1;
        }

        // Returns a copy of the provided array
        public static int[] CloneArray(int[] arr)
        {
            var clone = new int[arr.Length];
            for (var i = 0; i < arr.Length; i++)
            {
                clone[i] = arr[i];
            }
            return clone;
        }

        // Returns a sorted array, using the BubbleSort method
        public static void BubbleSort(int[] arr)
        {
            var n = arr.Length;
            if (n <= 1)
            {
                return;
            }
            while (n != 0)
            {
                var newn = 0;
                for (var i = 1; i < n; i++)
                {
                    if (arr[i - 1] <= arr[i])
                    {
                        continue;
                    }
                    Reference.Swap(ref arr[i - 1], ref arr[i]);
                    newn = i;
                }
                n = newn;
            }
        }

        // Sorts an array using the CombSort method: https://en.wikipedia.org/wiki/Comb_sort
        public static void CombSort(int[] arr)
        {
            var n = arr.Length;
            var interval = n;
            var sorted = true;
            while (interval > 1 || sorted)
            {
                interval = (int) (interval / 1.3);
                if (interval < 1)
                {
                    interval = 1;
                }
                sorted = false;
                for (var i = 0; i < n - interval; i++)
                {
                    if (arr[i] > arr[i + interval])
                    {
                        Reference.Swap(ref arr[i], ref arr[i + interval]);
                        sorted = true;
                    }
                }
            }
        }
    }
}