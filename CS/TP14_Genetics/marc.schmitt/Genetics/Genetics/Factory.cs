﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;


namespace Genetics
{
    public static class Factory
    {
        #region Attributes

        private static List<Player> _listPlayer;
        private static string _pathLoad;
        private static string _pathSave;

        #endregion

        #region Getters

        public static List<Player> GetListPlayer()
        {
            return _listPlayer;
        }

        public static void SetListPlayer(List<Player> li)
        {
            _listPlayer = li;
        }

        public static Player GetBestPlayer()
        {
            SimpleSort();
            return _listPlayer[_listPlayer.Count - 1];
        }

        public static Player GetNthPlayer(int nth)
        {
            if (nth < 0 || nth >= _listPlayer.Count) throw new IndexOutOfRangeException();
            SimpleSort();
            return _listPlayer[nth];
        }

        #endregion

        #region LoadAndSave

        public static void SetPathLoad(string path)
        {
            _pathLoad = path;
        }

        public static void SetPathSave(string path)
        {
            _pathSave = path;
        }

        public static void SetPathLoadAndSave(string path)
        {
            SetPathLoad(path);
            SetPathSave(path);
        }

        public static String GetPathLoad()
        {
            return _pathLoad;
        }
        public static String GetPathSave()
        {
            return _pathSave;
        }

        public static void SaveState()
        {
            SaveAndLoad.Save(_pathSave, _listPlayer);
        }

        #endregion

        #region Init

        public static void InitNew(int size = 200)
        {
            _listPlayer = new List<Player>(size);
            for (var i = 0; i < size; i++) _listPlayer.Add(new Player());
        }

        public static void Init()
        {
            try
            {
                _listPlayer = SaveAndLoad.Load(_pathLoad);
            }
            catch (Exception)
            {
                InitNew();
            }
        }

        #endregion

        #region Display

        public static void PrintScore()
        {
            for (var i = 0; i< _listPlayer.Count;i++) Console.WriteLine("Player {0} has a score of {1}",
                                                                        i, _listPlayer[i].GetScore());
        }

        #endregion

        #region Training

        public static void TrainWithNew(int generationNumber)
        {
            Train(generationNumber, false);
        }

        public static void Train(int generationNumber, bool replaceWithMutation = true)
        {
            for (var n = 0; n < generationNumber; n++)
            {
                Console.WriteLine(n);
                var threadList = new List<Thread>(Environment.ProcessorCount);
                for (var a = 0; a < _listPlayer.Count; a += _listPlayer.Count / Environment.ProcessorCount)
                {
                    var th = new Thread(new ParameterizedThreadStart(TrainThread));
                    th.Start(a);
                    threadList.Add(th);
                }

                foreach (var th in threadList) th.Join();
                Regenerate(replaceWithMutation);
            }
        }

        private static void TrainThread(object startIndex)
        {
            var sI = (int) startIndex;
            //Console.WriteLine("size: {0}, sI (inc): {1}, endI (not inc): {2}", _listPlayer.Count, sI, sI + _listPlayer.Count / Environment.ProcessorCount);
            for (var i = sI; i < sI + _listPlayer.Count / Environment.ProcessorCount; i++)
            {
                _listPlayer[i].ResetScore();
                var currentMap = RessourceLoad.GetCurrentMap();
                _listPlayer[i].SetStart(currentMap);
                for (var j = 0; j < currentMap.Timeout; j++) _listPlayer[i].PlayAFrame();
            }
        }

        private static void Regenerate(bool replace_with_mutation = true)
        {
            SimpleSort();
            for (var i = 0; i < _listPlayer.Count / 2; i++) _listPlayer[i].Replace(_listPlayer[i + _listPlayer.Count / 2], replace_with_mutation);
            //for (var i = 0; i < _listPlayer.Count / 2; i++) _listPlayer[i].Replace(_listPlayer[_listPlayer.Count - 1], replace_with_mutation);
        }

        private static void RegenerateThread(object replace_with_mutation)
        {
            //TODO
        }

        public static void SimpleSort()
        {
            var n = _listPlayer.Count;
            while (n != 0)
            {
                var newN = 0;
                for (var i = 1; i < n; i++)
                {
                    if (_listPlayer[i - 1] > _listPlayer[i])
                    {
                        var swap = _listPlayer[i - 1];
                        _listPlayer[i - 1] = _listPlayer[i];
                        _listPlayer[i] = swap;
                        newN = i;
                    }
                }
                n = newN;
            }
        }

        #endregion
    }
}