﻿using System;
using Debugger;

namespace TP2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //3.1.1
            //Loop.Print_Naturals(5);
            
            //3.1.2
            //Loop.Print_Primes(30);
            
            //3.1.3
            //Console.WriteLine(Loop.Fibonacci(1357));
            
            //3.1.4
            //Console.WriteLine(Loop.Factorial(23));
            
            //3.1.5
            //Loop.Print_Strong(150);
            
            //3.1.6
            //Console.WriteLine(Loop.Abs(-42));
            //Console.WriteLine(Loop.Sqrt(2));
            
            //3.1.7
            //Console.WriteLine(Loop.Power(2, 3));
            
            //3.1.8
            //Loop.Print_Tree(3);
            //Loop.Print_Tree(5);
            
            
            //3.2.1
            //Console.WriteLine(Debugging.ex1());
            
            //3.2.2
            //Console.WriteLine(Debugging.ex2(2, 5));
            
            //3.2.3
            //Console.WriteLine(Debugging.ex3(5));
            
            //3.2.4
            //Misc.printArr(Debugging.ex4(new int[] {3,6,1,82,18,6,42}));
        }
    }
}