﻿using System;
using System.Collections.Generic;

namespace Debugger
{
    public static class Loop
    {
        // 3.1.1
        // Prints all the integers from 1 to n
        public static void Print_Naturals(int n)
        {
            for (var i = 1; i <= n; i++)
            {
                if (i == n)
                {
                    Console.WriteLine(n);
                    break;
                }
                Console.Write(i + " ");
            }
        }

        // 3.1.2
        // Prints all the prime numbers from 1 to n
        public static void Print_Primes(int n)
        {
            for (var i = 1; i <= n; i++)
            {
                if (!Misc.IsPrime(i))
                {
                    continue;
                }
                
                if (i == n)
                {
                    Console.WriteLine(n);
                    break;
                }
                
                Console.Write(i + " ");
            }
        }

        // 3.1.3
        // Calculates the nth term of the Sibonacci sequence
        public static long Fibonacci(long n)
        {
            var m = 0;
            var p = 1;
            for (var i = 0; i < n - 1; i++)
            {
                var s = m;
                m = p;
                p += s;
            }
            return m;
        }

        // 3.1.4
        // Calculates n!
        public static long Factorial(long n)
        {
            if (n == 0)
            {
                return 1;
            }
            var f = 1;
            for (var i = 1; i <= n; i++)
            {
                f *= i;
            }
            return f;
        }

        // 3.1.5
        // Prints all the strong numbers between 1 and n
        public static void Print_Strong(int n)
        {
            for (var i = 1; i <= n; i++)
            {
                if (!Misc.IsStrong(i))
                {
                    continue;
                }
                if (i == n)
                {
                    Console.WriteLine(n);
                }
                Console.Write(i + " ");
            }
        }

        // 3.1.6
        // Calculates the absolute value of a float
        public static float Abs(float n)
        {
            return n < 0 ? -n : n;
        }

        // Calculates the square root of a float
        public static float Sqrt(float n)
        {
            double x = 1;
            for (var i = 0; i < 8; i++)
            {
                x = (x + n / x) / 2;

            }
            return (float)Math.Round(x, 5);
        }

        // 3.1.7
        // Calculates a to the power of b
        public static long Power(long a, long b)
        {
            if (b < 0)
            {
                throw new ArgumentException("power cannot be negative, because using long type, so no decimal number", nameof(b));
            }
            switch (b)
            {
                    case 0:
                        return 1;
                    case 1:
                        return a;
            }
            var p = a;
            for (var i = 1; i < Abs(b); i++)
            {
                p *= a;
            }
            return p;
        }

        // 3.1.8
        // Prints a nice and clean tree
        public static void Print_Tree(int n)
        {
            var fir = new List<string>();
            for (var i = 1; i <= n; i++)
            {
                fir.Add(Misc.GenerateString("*", i * 2 - 1));
            }
            var biggerBranch = fir[fir.Count - 1].Length;
            for (var i = 0; i < fir.Count; i++)
            {
                var indent = Misc.GenerateString(" ", (biggerBranch - fir[i].Length) / 2);
                fir[i] = indent + fir[i] + indent;
            }
            var trunkIndent = Misc.GenerateString(" ", biggerBranch / 2);
            if (n > 3)
            {
                fir.Add(trunkIndent + "*" + trunkIndent);
            }
            fir.Add(trunkIndent + "*" + trunkIndent);
            foreach (var line in fir)
            {
                Console.WriteLine(line);
            }
        }
    }
}