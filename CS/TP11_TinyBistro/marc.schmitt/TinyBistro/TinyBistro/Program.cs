﻿using System;

namespace TinyBistro
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("What type of operation do you want to use ?");
            Console.WriteLine("  1. +");
            Console.WriteLine("  2. -");
            Console.WriteLine("  3. *");
            Console.WriteLine("  4. /");
            Console.WriteLine("  5. %");
            Console.WriteLine();
            var res_n = "";
            var op = -1;
            do
            {
                Console.WriteLine("Please provide an integer between 1 and 5 ");
                res_n = Console.ReadLine();
                if (!int.TryParse(res_n, out op))
                {
                    Console.WriteLine("This was not an integer");
                    op = -1;
                }
            } while (op <= 0 || op > 5);

            int num1, num2;
            Console.WriteLine("Please type the first number");
            while (!int.TryParse(Console.ReadLine(), out num1))
                Console.WriteLine("This was not a number.\nPlease type the number again");
            Console.WriteLine();
            Console.WriteLine("Please type the second number");
            while (!int.TryParse(Console.ReadLine(), out num2))
                Console.WriteLine("This was not a number.\nPlease type the number again");

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Result: ");
            switch (op)
            {
                case 1:

                    var res1 = new BigNum(num1.ToString()) + new BigNum(num2.ToString());
                    res1.Print();
                    break;
                case 2:
                    var res2 = new BigNum(num1.ToString()) - new BigNum(num2.ToString());
                    res2.Print();
                    break;
                case 3:
                    var res3 = new BigNum(num1.ToString()) * new BigNum(num2.ToString());
                    res3.Print();
                    break;
                case 4:
                    var res4 = new BigNum(num1.ToString()) / new BigNum(num2.ToString());
                    res4.Print();
                    break;
                case 5:
                    var res5 = new BigNum(num1.ToString()) % new BigNum(num2.ToString());
                    res5.Print();
                    break;
                default:
                    throw new ArgumentException(
                        "Maybe you break the code of Main but this is not supposed to happen");
            }

            Console.WriteLine();
            Console.WriteLine("Press enter key to quit...");
            Console.ReadLine();
        }
    }
}