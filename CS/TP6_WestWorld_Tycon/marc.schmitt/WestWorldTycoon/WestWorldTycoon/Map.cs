﻿using System;
using System.CodeDom.Compiler;
using System.Runtime.InteropServices;

namespace WestWorldTycoon
{
    public class Map
    {
        private Tile[,] matrix;

        public Map(string name)
        {
            matrix = TycoonIO.ParseMap(name);
        }
        
        public Map(Map map)
        {
            matrix = map.matrix;
        }

        public bool Build(int i, int j, ref long money, Building.BuildingType type)
        {
            return matrix[i, j].Build(ref money, type);
        }

        public bool Destroy(int i, int j)
        {
            return matrix[i, j].Destroy();
        }

        public bool Upgrade(int i, int j, ref long money)
        {
            return matrix[i, j].Upgrade(ref money);
        }
        
        // get current attractiveness
        public long GetAttractiveness()
        {
            long attractiveness = 0;
            foreach (var tile in matrix)
            {
                attractiveness += tile.GetAttractiveness();
            }
            return attractiveness;
        }

        // get current housing
        public long GetHousing()
        {
            long housing = 0;
            foreach (var tile in matrix)
            {
                housing += tile.GetHousing();
            }
            return housing;
        }

        // get current population
        public long GetPopulation()
        {
            var housing = GetHousing();
            var attractiveness = GetAttractiveness();
            return housing > attractiveness ? attractiveness : housing;
        }
        
        // get current income
        public long GetIncome(long population)
        {
            long income = 0;
            foreach (var tile in matrix)
            {
                income += tile.GetIncome(population);
            }
            return income;
        }

        // check if a spot can be built on
        public bool IsBuildable(long i, long j)
        {
            return matrix[i, j].IsBuildable();
        }

        // return how much buildings of a BuildingType are built
        public long NbrBuildings(Building.BuildingType type)
        {
            long buildings = 0;
            foreach (var tile in matrix)
            {
                if (tile.IsType(type))
                {
                    buildings++;
                }
            }
            return buildings;
        }

        // get the size of the map
        public void GetSize(ref long i, ref long j)
        {
            i = matrix.GetLength(0);
            j = matrix.GetLength(1);
        }
    }
}