﻿using System;
using System.Xml.Schema;

namespace WestWorldTycoon
{
    public class Tile
    {
        public enum Biome { SEA, MOUNTAIN, PLAIN }

        private Biome biome;
        private Building building;

        public Tile(Biome b)
        {
            biome = b;
            building = null;
        }

        public Tile(Tile tile)
        {
            biome = tile.biome;
            building = tile.building;
        }

        // checks if the tile can be built on
        public bool IsBuildable()
        {
            return building == null && biome == Biome.PLAIN;
        }

        // checks if the building built on this tile is of a certain type
        public bool IsType(Building.BuildingType type)
        {
            switch (type)
            {
                case Building.BuildingType.ATTRACTION:
                    return building is Attraction;
                case Building.BuildingType.HOUSE:
                    return building is House;
                case Building.BuildingType.SHOP:
                    return building is Shop;
                case Building.BuildingType.NONE:
                    return false;
                default:
                    return false;
            }
        }

        public bool Build(ref long money, Building.BuildingType type)
        {
            if (!IsBuildable())
            {
                return false;
            }

            switch (type)
            {
                case Building.BuildingType.ATTRACTION:
                    if (money >= Attraction.BUILD_COST)
                    {
                        building = new Attraction();
                        money -= Attraction.BUILD_COST;
                        return true;
                    }
                    break;
                
                case Building.BuildingType.HOUSE:
                    if (money >= House.BUILD_COST)
                    {
                        building = new House();
                        money -= House.BUILD_COST;
                        return true;
                    }
                    break;
                
                case Building.BuildingType.SHOP:
                    if (money >= Shop.BUILD_COST)
                    {
                        building = new Shop();
                        money -= Shop.BUILD_COST;
                        return true;
                    }
                    break;
                
                default:
                    return false;
            }
            
            return false;
        }

        public bool Upgrade(ref long money)
        {
            if (building == null)
            {
                return false;
            }
            if (building is Attraction)
            {
                return ((Attraction) building).Upgrade(ref money);
            }
            if (building is House)
            {
                return ((House) building).Upgrade(ref money);
            }
            if (building is Shop)
            {
                return ((Shop) building).Upgrade(ref money);
            }
            return false;
        }

        public long GetHousing()
        {
            return building is House ? ((House) building).Housing() : 0;
        }

        public long GetAttractiveness()
        {
            return building is Attraction ? ((Attraction) building).Attractiveness() : 0;
        }

        public long GetIncome(long population)
        {
            return building is Shop ? ((Shop) building).Income(population) : 0;
        }

        public bool Destroy()
        {
            if (building == null)
            {
                return false;
            }
            building = null;
            return true;
        }

        public Biome GetBiome { get { return biome; } }
    }
}