let tomorrow date =
  let day = int_of_string (String.make 1 date.[0] ^ String.make 1 date.[1]) in
let month = int_of_string (String.make 1 date.[3] ^ String.make 1 date.[4]) in
let year = int_of_string (String.make 1 date.[6] ^ String.make 1 date.[7] ^ String.make 1 date.[8] ^ String.make 1 date.[9]) in 
if day < 1 || day > 31 || month < 1 || month > 12 then invalid_arg "not a valid day"
else
  if (month=2 || month=4 || month=6 || month=9 || month=11) && day = 31 then invalid_arg "not a valid day"
  else
    if day=31 && month=12 then let day=1 in let month=1 in let year=year+1 in ((string_of_int day) ^ "/" ^ (string_of_int month) ^ "/" ^ (string_of_int year))
    else
      if (day=30 && (month=2 || month=4 || month=6 || month=9 || month=11)) || day=31 then let day=1 in let month=month+1 in ((string_of_int day) ^ "/" ^ (string_of_int month) ^ "/" ^ (string_of_int year))
      else
	let day = day+1 in ((string_of_int day) ^ "/" ^ (string_of_int month) ^ "/" ^ (string_of_int year))

;;



tomorrow "08/09/2017";;
tomorrow "31/08/2017";;
tomorrow "31/12/2017";;
tomorrow "45/02/2005";;
tomorrow "13/13/1313";;
