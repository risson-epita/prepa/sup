(************ peculiar  **********)

let f n = n / 2 * 2 = n ;;
f 4 ;;
f 5 ;;

max_int ;;
max_int + 1 ;;
min_int ;;
min_int * 2 ;;
max_int * 2 ;;

exp (40.) = exp (40.) +. 1. ;;    (* Exponential function *)
exp (40.) = exp (40.) +. 31. ;;
exp (40.) = exp (40.) +. 32. ;;

1 / 0 ;;
1. /. 0. ;;
0. /. 0. ;;

false || (1/0 = 0) ;;
true || (1/0 = 0) ;;
false && (1/0 = 0) ;;
true && (1/0 = 0) ;;

'Z' < 'a' ;;
"A long sentence to see how string comparisons work" > "a little one" ;;

