(************************************************************)
(*                     Game of life                         *)
(************************************************************)

(* graphics *)

#load "graphics.cma" ;;
open Graphics ;;

let open_window size =
  open_graph(" " ^string_of_int size ^ "x" ^ string_of_int (size+20));;

let grey = rgb 127 127 127 ;;

let cell_color = function
  | 0 -> white
  | _ -> black ;;

let cell_size = 10 ;;

(* original game of life definitions *)

let new_cell = 1 ;;

let empty = 0 ;;

let is_alive cell = cell <> empty ;;


(*******************   FROM TP 3 *********************)
(*   insert here needed simple functions on lists    *)

let reverse n =
  let rec sub_rev n accu =
    if n=0 then accu
    else
      sub_rev (n/10) (accu * 10 + n mod 10)
  in sub_rev n 0
;;

let prime = function
  | 2 -> true
  | 3 -> true
  | x when x < 2 -> false
  | x when x mod 2 = 0 -> false
  | x when x mod 3 = 0 -> false
  | x -> try
	   for i = 3 to (int_of_float (sqrt (float_of_int x)))
	   do
	     if x mod i = 0
	     then raise Exit;
	   done;
	   true
    with Exit -> false
;;

let rec length = function
  | [] -> 0
  | [_] -> 1
  | e::rest -> 1 + length rest
;;

let rec count x l = match l with
  | [] -> 0
  | [e] -> if e = x then 1 else 0
  | e::rest -> if e = x then 1 + count x rest else count x rest
;;

let nth i l =
  if i < 0 then raise (Invalid_argument "i can't be negative")
  else if i >= length l then raise (Failure "nth: list is too short")
  else
    let rec nth_rec l i =
    match l with
      | [] -> raise (Failure "nth: list is empty")
      | e::rest -> if i = 0 then e else nth_rec rest (i-1)
    in nth_rec l i
;;

let rec search_pos x l =
  match l with
    | [] -> raise (Failure "search_pos: 404 not found")
    | e::rest -> if e = x then 0 else 1 + search_pos x rest
;;

let rec init_list n x =
  match n with
    | 0 -> []
    | n when n < 0 -> raise (Failure "init_list: n can't be negative")
    | n -> x::(init_list (n-1) x)
;;

let rec reverse_list l =
  let rec reverse_list_rec l inv =
    match l with
      | [] -> inv
      | e::rest -> reverse_list_rec rest (e::inv)
  in reverse_list_rec l []
;;



(*******************   Toolbox *********************)
(*             list list functions                 *)

let gen_board (l, c) v =
  init_list l (init_list c v) ;;

let get_cell (x, y) board =
  nth x (nth y board) ;;

let put_cell v (x, y) board =
  let rec process_rows n b b1 =
    match b with
      | [] -> b1
      | e::rest when n = x -> process_rows (n+1) rest (b1 @ [(process_cells 0 e [])])
      | e::rest -> process_rows (n+1) rest (b1 @ [e])
  and process_cells n b b1 =
    match b with
      | [] -> b1
      | e::rest when n = y -> process_cells (n+1) rest (b1 @ [v])
      | e::rest -> process_cells (n+1) rest (b1 @ [e])
  in process_rows 0 board []
;;

let count_neighbours (x, y) board (l, c) =
  let l = l-1 and c = c-1 in
  let value (x,y) = if (is_alive (get_cell (x, y) board)) then 1 else 0 in
		    match (x, y) with
		      | (0, 0) -> (value (1,0)) + (value (0,1)) + (value (1,1))
		      | (x, 0) when x = c -> (value (c-1,0)) + (value (c-1,1)) + (value (c,1))
		      | (0, y) when y = l -> (value (0,l-1)) + (value (1,l-1)) + (value (1,l))
		      | (x, y) when x = c && y = l -> (value (c-1,l-1)) + (value (c,l-1)) + (value (c-1,l))
		      | (x, y) when x = c -> (value (c-1,y-1)) + (value (c,y-1)) + (value (c-1,y)) + (value (c-1,y+1)) + (value (c,y+1))
		   
		      | (x, 0) -> (value (x-1,0)) + (value (x+1,0)) + (value (x-1,1)) + (value (x,1)) + (value (x+1,1))
		      
		      | (x, y) when y = l -> (value (x-1,l)) + (value (x-1,l-1)) + (value (x,l-1)) + (value (x+1,l-1)) + (value (x+1,l))
		     
		      | (0, y) -> (value (0,y-1)) + (value (1,y-1)) + (value (1,y)) + (value (1,y+1)) + (value (0,y+1))
		      | (x, y) -> (value (x-1,y-1)) + (value (x,y-1)) + (value (x+1,y-1)) + (value (x+1,y)) + (value (x+1,y+1)) + (value (x,y+1)) + (value (x-1,y+1)) + (value (x-1,y))
;;

(************************************************************)
(*                  graphics                                *)
(*        from the board to the graphic window              *)



(************************************************************)
(*                     Game of life                         *)
(************************************************************)



