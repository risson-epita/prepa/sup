let reverse n =
  let rec sub_rev n accu =
    if n=0 then accu
    else
      sub_rev (n/10) (accu * 10 + n mod 10)
  in sub_rev n 0
;;

let prime = function
  | 2 -> true
  | 3 -> true
  | x when x < 2 -> false
  | x when x mod 2 = 0 -> false
  | x when x mod 3 = 0 -> false
  | x -> try
	   for i = 3 to (int_of_float (sqrt (float_of_int x)))
	   do
	     if x mod i = 0
	     then raise Exit;
	   done;
	   true
    with Exit -> false
;;

let rec length = function
  | [] -> 0
  | [_] -> 1
  | e::rest -> 1 + length rest
;;

let rec count x l = match l with
  | [] -> 0
  | [e] -> if e = x then 1 else 0
  | e::rest -> if e = x then 1 + count x rest else count x rest
;;

let nth i l =
  if i < 0 then raise (Invalid_argument "i can't be negative")
  else if i >= length l then raise (Failure "nth: list is too short")
  else
    let rec nth_rec l i =
    match l with
      | [] -> raise (Failure "nth: list is empty")
      | e::rest -> if i = 0 then e else nth_rec rest (i-1)
    in nth_rec l i
;;

let rec search_pos x l =
  match l with
    | [] -> raise (Failure "search_pos: 404 not found")
    | e::rest -> if e = x then 0 else 1 + search_pos x rest
;;

let rec init_list n x =
  match n with
    | 0 -> []
    | n when n < 0 -> raise (Failure "init_list: n can't be negative")
    | n -> x::(init_list (n-1) x)
;;

let rec reverse_list l =
  let rec reverse_list_rec l inv =
    match l with
      | [] -> inv
      | e::rest -> reverse_list_rec rest (e::inv)
  in reverse_list_rec l []
;;

let rec trajectory x n =
  match n with
    | 0 -> []
    | n -> x::trajectory (x + (reverse x)) (n-1)
;;

let goldbach x =
  if x mod 2 = 1 then raise (Failure "goldbach: number: input must be even") else 
  let rec goldbach_rec n =
    if (prime n) then n
    else goldbach_rec (n-1)
  in
  let p = goldbach_rec (x-1) in
  (p, x-p)
;;

let goldbach_list a b =
  
