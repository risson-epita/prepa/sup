#load "graphics.cma" ;;
open Graphics ;;
open_graph "" ;;
resize_window 1000 500 ;;
set_color blue;;

let rec flocon n (x,y,z,t) =
  let all_segments = [];
  let segment (x,y,z,t) =
      let a_x = (x+z)/3;
      let a_y = (y+t)/3;
      let b_x = 2*(x+z)/3;
      let b_y = 2*(y+t)/3;
      let d = sqrt(float_of_int((b_x-a_x)*(b_x-a_x)+(b_y-a_y)*(b_y-a_y)));
      let c_x = a_x + int_of_float(d*.cos(60.0));
      let c_y = a_y + int_of_float(d*.sin(60.0));
      moveto x y;
      print_string "tracing";
      lineto a_x b_x;
      lineto c_x c_y;
      lineto b_x b_y;
      lineto z t;
      moveto a_x b_x;
      lineto b_x b_y;
      set_color blue;
      let all_segments = List.append all_segments [(x,y,a_x,b_x);(a_x,b_x,c_x,c_y);(c_x,c_y,b_x,b_y);(b_x,b_y,z,t)];
  let all_segments = List.append all_segments [(x,y,z,t)];
  let rec go_through_segments list = match list with
        [] -> ()
      | head::body ->
	begin
	  segment(head);
	  go_through_segments body
	end;
	let ijk = n in
    while not (ijk=0) do
      print_string "start";
      go_through_segments(all_segments);
      print_int ijk;
      let ijk = ijk-1;
      
      done;;

flocon 5 (150,150,1000,150);;
